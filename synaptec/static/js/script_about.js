var addEvent = function(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on"+type] = callback;
    }
};

addEvent(window, "resize", function(event) {
    var h = window.innerHeight;
    var w = window.innerWidth;
    if (w > h & w < 768 & h < 440) {
        document.getElementById("img-xs").className = "about-mobile-xs"
        document.getElementById("glasses-div").className = "hidden-lg hidden-md hidden-sm div-to-center div-margin-top-xs"
        document.getElementById("foot1").className = "icons col-md-3 col-xs-4 col-xs-push-8"
        document.getElementById("foot2").className = "col-md-6 col-xs-4"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-xs-4 col-xs-pull-8"
        document.getElementById("we").className = "montserrat-light deeply-xs"
        document.getElementById("care").className = "montserrat-light deeply-xs"
        document.getElementById("very").className = "montserrat-light deeply-xs"
        document.getElementById("deeply").className = "montserrat-light deeply-xs"
    } else {
        document.getElementById("img-xs").className = "about-mobile"
        document.getElementById("glasses-div").className = "hidden-lg hidden-md hidden-sm div-to-center"
        document.getElementById("foot1").className = "icons col-md-3 col-sm-4 col-sm-push-8"
        document.getElementById("foot2").className = "col-md-6 col-sm-4 hidden-xs"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-sm-4 col-sm-pull-8"
        document.getElementById("we").className = "montserrat-light deeply"
        document.getElementById("care").className = "montserrat-light deeply"
        document.getElementById("very").className = "montserrat-light deeply"
        document.getElementById("deeply").className = "montserrat-light deeply"
    }
});

addEvent(window, "load", function(event) {
    var h = window.innerHeight;
    var w = window.innerWidth;
    if (w > h & w < 768 & h < 440) {
        document.getElementById("img-xs").className = "about-mobile-xs"
        document.getElementById("glasses-div").className = "hidden-lg hidden-md hidden-sm div-to-center div-margin-top-xs"
        document.getElementById("foot1").className = "icons col-md-3 col-xs-4 col-xs-push-8"
        document.getElementById("foot2").className = "col-md-6 col-xs-4"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-xs-4 col-xs-pull-8"
        document.getElementById("we").className = "montserrat-light deeply-xs"
        document.getElementById("care").className = "montserrat-light deeply-xs"
        document.getElementById("very").className = "montserrat-light deeply-xs"
        document.getElementById("deeply").className = "montserrat-light deeply-xs"
    } else {
        document.getElementById("img-xs").className = "about-mobile"
        document.getElementById("glasses-div").className = "hidden-lg hidden-md hidden-sm div-to-center"
        document.getElementById("foot1").className = "icons col-md-3 col-sm-4 col-sm-push-8"
        document.getElementById("foot2").className = "col-md-6 col-sm-4 hidden-xs"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-sm-4 col-sm-pull-8"
        document.getElementById("we").className = "montserrat-light deeply"
        document.getElementById("care").className = "montserrat-light deeply"
        document.getElementById("very").className = "montserrat-light deeply"
        document.getElementById("deeply").className = "montserrat-light deeply"
    }
});

// window.location.reload(false); 